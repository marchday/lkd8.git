//
//  LKD8AppDelegate.h
//  LKD8
//
//  Created by LK on 09/23/2020.
//  Copyright (c) 2020 LK. All rights reserved.
//

@import UIKit;

@interface LKD8AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
