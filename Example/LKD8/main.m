//
//  main.m
//  LKD8
//
//  Created by LK on 09/23/2020.
//  Copyright (c) 2020 LK. All rights reserved.
//

@import UIKit;
#import "LKD8AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LKD8AppDelegate class]));
    }
}
