# LKD8

[![CI Status](https://img.shields.io/travis/LK/LKD8.svg?style=flat)](https://travis-ci.org/LK/LKD8)
[![Version](https://img.shields.io/cocoapods/v/LKD8.svg?style=flat)](https://cocoapods.org/pods/LKD8)
[![License](https://img.shields.io/cocoapods/l/LKD8.svg?style=flat)](https://cocoapods.org/pods/LKD8)
[![Platform](https://img.shields.io/cocoapods/p/LKD8.svg?style=flat)](https://cocoapods.org/pods/LKD8)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LKD8 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LKD8'
```

## Author

LK, 184049708@qq.com

## License

LKD8 is available under the MIT license. See the LICENSE file for more info.
