

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "MangerSing.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetWorkManager : NSObject
//封装GET方法
+(void)getWithURL:(NSString *)url parameter:(NSDictionary *)para completeHandle:(void(^)(id data, NSError *error))handle;
//封装POST方法
+(void)postWithURL:(NSString *)url parameter:(NSDictionary *)para completeHandle:(void(^)(id data, NSError *error))handle;
//检测是否挂了代理
+ (BOOL)getProxyStatusWithURL:(NSString *)url;
@end
NS_ASSUME_NONNULL_END
