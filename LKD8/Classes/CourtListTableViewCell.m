//
//  CourtListTableViewCell.m
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import "CourtListTableViewCell.h"
#import "LKD8Base.h"

@implementation CourtListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setFrame:(CGRect)frame{
    frame.origin.y += 5;
    frame.size.height -= 10;
    super.frame = frame;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    //球场图片
    _courtImg = [[UIImageView alloc]initWithFrame:CGRectMake(15*kScale, 15*kScale, 78.5*kScale, 78.5*kScale)];
    _courtImg.layer.cornerRadius = 39.5*kScale;
    _courtImg.layer.masksToBounds = YES;
    [self addSubview:_courtImg];
    
    //球场名
    _courtName = [[UILabel alloc]initWithFrame:CGRectMake(103.5*kScale, 15*kScale, 136*kScale, 18.5*kScale)];
    _courtName.textColor = [UIColor whiteColor];
    _courtName.font = [UIFont systemFontOfSize:13];
    [self addSubview:_courtName];
    
    //球场类型
    _courtType = [[UILabel alloc]initWithFrame:CGRectMake(103.5*kScale, 46*kScale, 136*kScale, 16.5*kScale)];
    _courtType.textColor = [UIColor whiteColor];
    _courtType.font = [UIFont systemFontOfSize:12];
    [self addSubview:_courtType];
    
    //球场简介
    _courtTips = [[UILabel alloc]initWithFrame:CGRectMake(103.5*kScale, 75*kScale, 130*kScale, 16.5*kScale)];
    _courtTips.textColor = [UIColor whiteColor];
    _courtTips.font = [UIFont systemFontOfSize:12];
    [self addSubview:_courtTips];
    
    //价格
    _courtPrice = [[UILabel alloc]initWithFrame:CGRectMake(233.5*kScale, 75*kScale, 66.5*kScale, 16.5*kScale)];
    _courtPrice.textColor = [UIColor whiteColor];
    _courtPrice.font = [UIFont systemFontOfSize:12];
    [self addSubview:_courtPrice];
    
    //地址
    _courtAddress = [[UIButton alloc]initWithFrame:CGRectMake(16*kScale, 111.5*kScale, 265.5*kScale, 16*kScale)];
    [_courtAddress setImage:[LKD8BaseImg zlc_imageNamed:@"dingwei@2x" ofType:@".png"] forState:UIControlStateNormal];
    [_courtAddress setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_courtAddress setTitleEdgeInsets:UIEdgeInsetsMake(0, 10*kScale, 0, -10*kScale)];
    _courtAddress.titleLabel.font = [UIFont systemFontOfSize:12];
    _courtAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _courtAddress.userInteractionEnabled = NO;
    [self addSubview:_courtAddress];
    
    //收藏
    _myColltionBtn = [[UIButton alloc]initWithFrame:CGRectMake(268*kScale, 15.5*kScale, 17.4*kScale, 16.72*kScale)];
    //[_myColltionBtn setImage:[UIImage imageNamed:@"weishoucang"] forState:UIControlStateNormal];
    [self addSubview:_myColltionBtn];
    
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.16];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    return self;
}

@end
