

#import "MangerSing.h"

@implementation MangerSing
+ (AFHTTPSessionManager *)sharedHTTPSession{
    static  AFHTTPSessionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
        //        manager.requestSerializer.timeoutInterval = 30;
        //        [manager.requestSerializer  setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    });
    return manager;
}
@end
