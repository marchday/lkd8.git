//
//  CourtListTableViewCell.h
//  LKD8
//
//  Created by Nuo A on 2020/9/22.
//  Copyright © 2020 cjf. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourtListTableViewCell : UITableViewCell
@property (nonatomic,strong)UIImageView *courtImg;
@property (nonatomic,strong)UILabel *courtName;
@property (nonatomic,strong)UILabel *courtType;
@property (nonatomic,strong)UILabel *courtTips;
@property (nonatomic,strong)UILabel *courtPrice;
@property (nonatomic,strong)UIButton *courtAddress;
@property (nonatomic,strong)UIButton *myColltionBtn;
@end

NS_ASSUME_NONNULL_END
