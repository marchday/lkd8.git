//
//  CourtDateilsViewController.h
//  LKD8
//
//  Created by Nuo A on 2020/9/23.
//  Copyright © 2020 cjf. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourtDateilsViewController : UIViewController
@property (nonatomic,strong)NSDictionary *courtDic;
@end

NS_ASSUME_NONNULL_END
