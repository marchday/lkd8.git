

#import "NetWorkManager.h"

@implementation NetWorkManager
{
    AFHTTPSessionManager *manager;
}
+(void)getWithURL:(NSString *)url parameter:(NSDictionary *)para completeHandle:(void (^)(id, NSError *))handle
{
    BOOL isProxy = [self getProxyStatusWithURL:url];
    if (!isProxy) {
        //创建封装NSURLSession的管理对象
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        //设置二进制解析器
        //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSMutableSet *newSet = [NSMutableSet set];
        // 添加我们需要的类型
        newSet.set = manager.responseSerializer.acceptableContentTypes;
        [newSet addObject:@"text/html"];
        // 重写给 acceptableContentTypes赋值
        manager.responseSerializer.acceptableContentTypes = newSet;
        
        [manager GET:url parameters:para progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            handle(responseObject, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            handle(nil, error);
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"警告" message:@"检测到网络异常" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

+ (BOOL)getProxyStatusWithURL:(NSString *)url {
    //    NSDictionary *proxySettings =  (__bridge NSDictionary *)(CFNetworkCopySystemProxySettings());
    NSDictionary *proxySettings =  (__bridge_transfer NSDictionary *)(CFNetworkCopySystemProxySettings());
    
    //    NSArray *proxies = (__bridge NSArray *)(CFNetworkCopyProxiesForURL((__bridge CFURLRef _Nonnull)([NSURL URLWithString:url]), (__bridge CFDictionaryRef _Nonnull)(proxySettings)));
    NSArray *proxies = (__bridge_transfer NSArray *)(CFNetworkCopyProxiesForURL((__bridge CFURLRef _Nonnull)([NSURL URLWithString:url]), (__bridge CFDictionaryRef _Nonnull)(proxySettings)));
    NSDictionary *settings = [proxies objectAtIndex:0];
    
//    NSLog(@"host=%@", [settings objectForKey:(NSString *)kCFProxyHostNameKey]);
//    NSLog(@"port=%@", [settings objectForKey:(NSString *)kCFProxyPortNumberKey]);
//    NSLog(@"type=%@", [settings objectForKey:(NSString *)kCFProxyTypeKey]);
    
    if ([[settings objectForKey:(NSString *)kCFProxyTypeKey] isEqualToString:@"kCFProxyTypeNone"]){
        //没有设置代理
        return NO;
    }else{
        //设置代理了
        return YES;
    }
}



+(void)postWithURL:(NSString *)url parameter:(NSDictionary *)para completeHandle:(void (^)(id, NSError *))handle
{
    BOOL isProxy = [self getProxyStatusWithURL:url];
    if (!isProxy) {
        NSString *keyUrl = @"www.";
        NSMutableString *urlStr = [NSMutableString stringWithString:url];
        if (![url containsString:keyUrl]) {
            [urlStr insertString:keyUrl atIndex:8];
            
        }
        //创建封装NSURLSession的管理对象
        AFHTTPSessionManager *manager = [MangerSing sharedHTTPSession];
        //设置二进制解析器
        //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSMutableSet *newSet = [NSMutableSet set];
        // 添加我们需要的类型
        newSet.set = manager.responseSerializer.acceptableContentTypes;
        [newSet addObject:@"text/html"];
//        NSLog(@"uuuurl = %@",urlStr);
        
        // 重写给 acceptableContentTypes赋值
        manager.responseSerializer.acceptableContentTypes = newSet;
         
        [manager POST:urlStr parameters:para progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //成功回调
            handle(responseObject, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //失败回调
            handle(nil, error);
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"警告" message:@"检测到网络异常" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
@end
